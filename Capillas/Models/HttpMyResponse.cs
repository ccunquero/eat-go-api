﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Capillas.Models
{
    public class HttpMyResponse
    {
        public Int32 code { get; set; }
        public object data { get; set; }
    }
}
