﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Capillas.Models
{
    public class Parametros
    {
        public string NombreParametro { get; set; }
        public Object Valor { get; set; }
        //public List<SqlParameter> parametros { get; set; }
    }

    public class ParametrosCollection
    {
        public string Objeto { get; set; }
        public List<Parametros> Parametros { get; set; }
    //public List<SqlParameter> parametros { get; set; }
}
}
