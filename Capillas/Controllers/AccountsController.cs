﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Security.Data;
using Security.Helpers;
using Security.Models.Entities;
using Security.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Identity;

using Security;
using System.Data.SqlClient;
using Security.Models;

namespace Capillas.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        public AccountsController()
        {
        
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]RegistrationViewModel model)
        {
            // simulate slightly longer running operation to show UI state change
            await Task.Delay(250);
            List<SqlParameter> lstParametros = new List<SqlParameter>();
            IdentityResult result = null;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                DbResult res = new UserRepository().AddUser(model);
                   return new OkObjectResult(res);
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(Errors.AddErrorsToModelState(result, ModelState));
            }

        }
    }
}
