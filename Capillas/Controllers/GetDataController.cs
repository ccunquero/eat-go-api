﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using Capillas.Models;
using System.Data;
using Newtonsoft.Json;
using Security.Data;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json.Linq;

namespace Capillas.Controllers
{
    //[Authorize(Policy = "ApiUser")]
    [Route("api/[controller]")]
    [ApiController]
    public class GetDataController : MyControllerBase
    {

        List<SqlParameter> lstParametros = new List<SqlParameter>();
        DbAcces db = new DbAcces();
        private IHostingEnvironment _hostingEnvironment;



        public GetDataController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        // GET api/values
        [HttpGet("GetTest")]
        public ActionResult<IEnumerable<string>> GetTest()
        {
            return new string[] { "value11", "value21" };
        }

        [HttpPost("ExecuteSp_DT")]
        public IActionResult ExecuteDataTable(ParametrosCollection Paramtros)
        {
            HttpMyResponse response = new HttpMyResponse();
            DataTable dt = new DataTable();
            lstParametros.Clear();
            try
            {
                foreach (Parametros item in Paramtros.Parametros)
                {
                    lstParametros.Add(new SqlParameter("@" + item.NombreParametro, item.Valor));
                }
                dt = db.Ejecutar_StoredProcedureDT(Paramtros.Objeto, lstParametros);
                response.code = 200;
                response.data = dt;
                object json = JsonConvert.SerializeObject(response, Formatting.Indented);
                return new OkObjectResult(json);
            }
            catch (Exception ex)
            {
                ErrorHttpResponse error = new ErrorHttpResponse();
                error.code = 500;
                error.errormsg = ex.Message;
                object json = JsonConvert.SerializeObject(error, Formatting.Indented);
                return InternalServerError(json);
            }
        }
        [HttpPost("ExecuteSp_DS")]
        public IActionResult ExecuteSP(ParametrosCollection parametros)
        {
            HttpMyResponse response = new HttpMyResponse();
            DataSet ds = new DataSet();
            lstParametros.Clear();
            try
            {
                foreach (Parametros item in parametros.Parametros)
                {
                    lstParametros.Add(new SqlParameter("@" + item.NombreParametro, item.Valor));
                }
                ds = db.Ejecutar_StoredProcedureDS(parametros.Objeto, lstParametros);
                response.code = 200;
                response.data = ds;
                object json = JsonConvert.SerializeObject(response, Formatting.Indented);
                return new OkObjectResult(json);
            }
            catch (Exception ex)
            {
                ErrorHttpResponse error = new ErrorHttpResponse();
                error.errormsg = ex.Message;
                error.code = 500;
                // object json = JsonConvert.SerializeObject(error, Formatting.Indented);
                return InternalServerError(error);
            }
        }
        [HttpPost("execute")]
        public IActionResult Ejecutar(Dictionary<string, object> desirializedJsonObject)
        {
            string nombreSP = "";
            bool isMenu = false;
            HttpMyResponse response = new HttpMyResponse();
            DataSet ds = new DataSet();

            lstParametros.Clear();
            //

            foreach (var obj in desirializedJsonObject)
            {
                if (obj.Key == "sp")
                {
                    nombreSP = obj.Value.ToString();
                }
                if (obj.Key == "model")
                {
                    string jsonString = obj.Value.ToString();
                    if (jsonString != "[]")
                    {
                        Dictionary<string, object> Params = JsonConvert.DeserializeObject<Dictionary<string, object>>(obj.Value.ToString());
                        foreach (var item in Params)
                        {
                            if (item.Key.Substring(0, 2) != "$$" && item.Key.Substring(0, 1) != "$")//variables especiales que no deberian enviarse
                            {
                                lstParametros.Add(new SqlParameter("@" + item.Key, item.Value));
                                continue;
                            }
                            if (item.Key.Substring(0, 1) == "$" && item.Key.Substring(0, 2) != "$$")//variables especiales que no deberian enviarse
                            {
                                lstParametros.Add(new SqlParameter("@" + item.Key.Replace("$", ""), item.Value));
                            }


                        }
                    }
                }
                if (obj.Key == "isMenu")
                {
                    isMenu = true;
                }

            }

            try
            {
                ds = db.Ejecutar_StoredProcedureDS(nombreSP, lstParametros);
                if (ds.Tables.Count == 1)
                    response.data = ds.Tables[0];
                else
                    response.data = ds;

                response.code = 200;

                if (isMenu)
                {
                    response.data = DataMenu(ds);
                    object jsonMenu = JsonConvert.SerializeObject(response, Formatting.Indented);
                    return new OkObjectResult(jsonMenu);

                }
                object json = JsonConvert.SerializeObject(response, Formatting.Indented);
                return new OkObjectResult(json);
            }
            catch (Exception ex)
            {
                ErrorHttpResponse error = new ErrorHttpResponse();
                error.errormsg = ex.Message;
                error.code = 500;
                // object json = JsonConvert.SerializeObject(error, Formatting.Indented);
                return InternalServerError(error);
            }
        }
        [HttpPost("upload-file-single")]
        public IActionResult UploadFileSingle()
        {
            try
            {
               
                IFormFileCollection files = Request.Form.Files;
                var appSettingsJson = AppSettingsJson.GetAppSettings();
                string baseFilesRoute = appSettingsJson["PathMediaApp"].ToString();

                string fileRoutes = GetFiles(files, baseFilesRoute, "PICTURES");
                string jsonString =@"{'Path_file' : '" + fileRoutes + "'}";


                object json = JObject.Parse(jsonString);
                return new OkObjectResult(json);
            }
            catch (Exception ex) 
            {
         
                ErrorHttpResponse error = new ErrorHttpResponse();
                error.errormsg = ex.Message;
                error.code = 500;
                // object json = JsonConvert.SerializeObject(error, Formatting.Indented);
                return InternalServerError(error);
                
            }
           
        }
        [HttpPost("file-base64")]
        public IActionResult getFileBase64(Dictionary<string, object> desirializedJsonObject)
        {
          
            try
            {

                String Base64 = String.Empty;
                var appSettingsJson = AppSettingsJson.GetAppSettings();
                string baseFilesRoute = appSettingsJson["PathMediaApp"].ToString();
                foreach (var obj in desirializedJsonObject)
                {
                    string fullPath = Path.Combine(baseFilesRoute, obj.Value.ToString());
                    Byte[] bytes = System.IO.File.ReadAllBytes(fullPath);
                    Base64 = Convert.ToBase64String(bytes);

                }
                string jsonString = @"{'File' : '" + Base64 + "'}";
                object json = JObject.Parse(jsonString);
                return new OkObjectResult(json);

            }
            catch (Exception ex)
            {
                ErrorHttpResponse error = new ErrorHttpResponse();
                error.errormsg = ex.Message;
                error.code = 500;
                // object json = JsonConvert.SerializeObject(error, Formatting.Indented);
                return InternalServerError(error);
            }

        }

        [HttpPost("upload-file")]
        public IActionResult UploadFiles()
        {
            DataSet ds = new DataSet();
            HttpMyResponse response = new HttpMyResponse();
            var form = Request.Form;
            IFormFileCollection files = Request.Form.Files;

            string nombreSP = "";
            string rutaMedia = string.Empty;
            var appSettingsJson = AppSettingsJson.GetAppSettings();
            string baseFilesRoute = appSettingsJson["PathMediaApp"].ToString();
            string filesToBd = string.Empty;

            lstParametros.Clear();
            foreach (var key in form.Keys) 
            {
                var jsonData = form[key.ToString()];
                Dictionary<string, object> dataSerialized = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonData);

                foreach (var item in dataSerialized)
                {
                    if (item.Key == "spr")
                    {
                        nombreSP = item.Value.ToString();
                    }

                    if (item.Key == "modelo")
                    {
                        var jsonParams = item.Value.ToString();
                        if (jsonParams != "[]")
                        {
                            //List<Dictionary<string, object>> Params = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(jsonParams);
                            Dictionary<string, object> Params = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonParams);
                            foreach (var param in Params)
                            {
                                if (param.Key.Substring(0, 2) != "$$" && param.Key.Substring(0, 1) != "$")//variables especiales que no deberian enviarse
                                {
                                    lstParametros.Add(new SqlParameter("@" + param.Key, param.Value));
                                    continue;
                                }

                                if (param.Key.Substring(0, 1) == "$" && param.Key.Substring(0, 2) != "$$")//variables especiales que no deberian enviarse
                                {
                                    rutaMedia = param.Value.ToString();
                                    string folderFileRoute = param.Value.ToString();
                                    string fileRoutes = GetFiles(files, baseFilesRoute, folderFileRoute);
                                    lstParametros.Add(new SqlParameter("@" + param.Key.Replace("$", ""), fileRoutes));
                                }


                            }
                        }
                    }

                }
            }

            try
            {
                ds = db.Ejecutar_StoredProcedureDS(nombreSP, lstParametros);
                if (ds.Tables.Count == 1)
                    response.data = ds.Tables[0];
                else
                    response.data = ds;
                response.code = 200;

                object json = JsonConvert.SerializeObject(response, Formatting.Indented);
                return new OkObjectResult(json);
            }
            catch (Exception ex)
            {
                ErrorHttpResponse error = new ErrorHttpResponse();
                error.errormsg = ex.Message;
                error.code = 500;
                // object json = JsonConvert.SerializeObject(error, Formatting.Indented);
                return InternalServerError(error);
            }
        }
        private string GetFiles(IFormFileCollection files, string baseFilesRoute, string folderFileRoute)
        {
            string fileCollection = string.Empty;
            foreach (var file in files)
                try
                {
                    //string webRootPath = rutaMedia; //_hostingEnvironment.WebRootPath;
                    string newPath = Path.Combine(baseFilesRoute, folderFileRoute);
                    if (!Directory.Exists(newPath))
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    if (file.Length > 0)
                    {
                        string tmpName = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
                        string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                        fileName = tmpName + "_" + fileName;
                        string fullPath = Path.Combine(newPath, fileName);
                        if (fileCollection == string.Empty)
                            fileCollection = folderFileRoute + "/" + fileName;
                        else
                            fileCollection = fileCollection + "|" + folderFileRoute + "/" + fileName;
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }

                    }

                }
                catch (System.Exception ex)
                {
                    return ex.Message;
                }
            return fileCollection;
        }

        private object DataMenu(DataSet data)
        {
          
            DataTable idCards = data.Tables[0];
            DataTable menus = data.Tables[1];
            Dictionary<string, object> singleSon;
            List<object> allSon = new List<object>();
            Dictionary<string, object> singleMenu;
            List<object> detailMenu;

            Dictionary<string, object> date;

            foreach (DataRow item in idCards.Rows)
            {
                singleSon = new Dictionary<string, object>
                {
                    { "id_son", item["id_card"] },
                    { "name", item["name_son"] }
                };

                DataRow[] resultMenu = menus.Select("id_card = " + Convert.ToInt32(item["id_card"]));
                detailMenu = new List<object>();
                foreach (DataRow dr in resultMenu)
                {
                    date = new Dictionary<string, object>
                    {
                        {"date" , dr["date_menu"]}
                    };

                    singleMenu = new Dictionary<string, object>
                    {

                        {"id" , dr["id_menu"] },
                        {"summary", dr["name"] },
                        {"description", dr["description"] },
                        {"start", date },
                        {"end", date },
                        {"id_detail_menu", dr["id_detail_menu"] } 
                    };
                    detailMenu.Add(singleMenu);

                }
                singleSon.Add("menus", detailMenu);
                allSon.Add(singleSon);

            };

            return allSon;


        }
        private string GenerateID()
        {
            return Guid.NewGuid().ToString("N");
        }



    }

}
