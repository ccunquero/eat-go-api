﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Security.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Security.Data;
namespace Capillas.Controllers
{
   // [Authorize(Policy = "ApiUser")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        private readonly ClaimsPrincipal _caller;
        public DashboardController(IHttpContextAccessor httpContextAccessor)
        {
            _caller = httpContextAccessor.HttpContext.User;
        }

        // GET api/dashboard/home
        [HttpGet]
        public IActionResult Home()
        {
            // retrieve the user info
            var userId = _caller.Claims.Single(c => c.Type == "id");
            int id_User = Convert.ToInt32(userId.Value);
            AppUser user = new AppUser();
            user = new UserRepository().GetUser(Convert.ToInt32(userId.Value));
            return new OkObjectResult(user);

        }

    }
}
