﻿
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Security.Data;
using Security.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;

namespace Capillas.Controllers
{
    [Authorize(Policy = "ApiUser")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProfileController : ControllerBase
    {
        private readonly ClaimsPrincipal _caller;
      
        public ProfileController(IHttpContextAccessor httpContextAccessor)
        {
            _caller = httpContextAccessor.HttpContext.User;
        }

        // GET api/profile/me
        [HttpGet]
        public IActionResult Me()
        {
            var userId = _caller.Claims.Single(c => c.Type == "id");
            AppUser user = new AppUser();
            user = new UserRepository().GetUser(Convert.ToInt32(userId.Value));
            return new OkObjectResult(user);
        }

    }
}
