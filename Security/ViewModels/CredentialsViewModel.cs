﻿
using Security.ViewModels.Validations;
using FluentValidation.Attributes;

namespace Security.ViewModels
{
  [Validator(typeof(CredentialsViewModelValidator))]
  public class CredentialsViewModel
  {
    public string UserName { get; set; }
    public string Password { get; set; }
  }
}
