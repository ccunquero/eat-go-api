﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Security.Data
{
    public class DbAcces
    {

        private string getConnectionString()
        {
            var appSettingsJson = AppSettingsJson.GetAppSettings();
            return appSettingsJson["ConnectionStrings:DefaultConnection"].ToString();

        }


        public DataTable Ejecutar_StoredProcedureDT(String sStoredProcedure, List<SqlParameter> ListaParametros)
        {
            string cn = getConnectionString();
            SqlConnection Cnx = new SqlConnection();
            SqlCommand Cmnd = new SqlCommand();
            SqlDataAdapter AdapterSql = new SqlDataAdapter();
            DataTable dt = new DataTable();
            try
            {
                Cnx = new SqlConnection(getConnectionString());
                Cmnd = new SqlCommand(sStoredProcedure, Cnx);
                Cmnd.CommandType = CommandType.StoredProcedure;
                Cmnd.CommandTimeout = 600;

                if (!(ListaParametros == null))
                {
                    foreach (SqlParameter Fila in ListaParametros)
                    {
                        if (Fila.Value == null)
                        {
                            Cmnd.Parameters.AddWithValue(Fila.ParameterName, DBNull.Value);
                        }
                        else
                        {
                            Cmnd.Parameters.Add(Fila);
                        }
                    }
                }

                Cnx.Open();
                AdapterSql.SelectCommand = Cmnd;
                AdapterSql.Fill(dt);
            }

            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                Cnx.Close();
                Cnx.Dispose();
                Cmnd.Dispose();
                AdapterSql.Dispose();
            }
            return dt;
        }
        public DataSet Ejecutar_StoredProcedureDS(String sStoredProcedure, List<SqlParameter> ListaParametros)
        {
            SqlConnection Cnx = new SqlConnection();
            SqlCommand Cmnd = new SqlCommand();
            SqlDataAdapter AdapterSql = new SqlDataAdapter();
            DataSet ds = new DataSet();

            try
            {
                Cnx = new SqlConnection(getConnectionString());
                Cmnd = new SqlCommand(sStoredProcedure, Cnx);
                Cmnd.CommandType = CommandType.StoredProcedure;
                Cmnd.CommandTimeout = 600;

                if (!(ListaParametros == null))
                {
                    foreach (SqlParameter Fila in ListaParametros)
                    {
                        if (Fila.Value == null)
                        {
                            Cmnd.Parameters.AddWithValue(Fila.ParameterName, DBNull.Value);
                        }
                        else
                        {
                            Cmnd.Parameters.Add(Fila);
                        }
                    }
                }

                Cnx.Open();
                AdapterSql.SelectCommand = Cmnd;
                AdapterSql.Fill(ds);


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally
            {
                Cnx.Close();
                Cnx.Dispose();
                Cmnd.Dispose();
                AdapterSql.Dispose();
            }
            return ds;
        }
    }
}
