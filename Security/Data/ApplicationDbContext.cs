﻿
using Security.Models.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace Security.Data
{
  public class ApplicationDbContext : IdentityDbContext<AppUser, IdentityRole<int>, int>
    {
    public ApplicationDbContext(DbContextOptions options)
      : base(options)
    {
    }

    public DbSet<Customer> Customers { get; set; }
  }
}
