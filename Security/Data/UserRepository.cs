﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Security.Models.Entities;
using Security.ViewModels;
using Security.Models;
namespace Security.Data
{
   public class UserRepository
    {
        public AppUser LogIn(CredentialsViewModel credentials)
        {
            List<SqlParameter> lstParametros = new List<SqlParameter>();
            AppUser user = new AppUser();
            lstParametros.Add(new SqlParameter("@usuario", credentials.UserName));
            lstParametros.Add(new SqlParameter("@contrasenia", credentials.Password));

            DataTable dt = new DbAcces().Ejecutar_StoredProcedureDT("spr_sel_autenticacion", lstParametros);
            if (dt.Rows.Count > 0)
            {
                user.Id = Convert.ToInt32(dt.Rows[0]["id_usuario"].ToString());
                user.UserName = Convert.ToString(dt.Rows[0]["usuario"].ToString());
                user.FirstName = dt.Rows[0]["descripcion"].ToString();
                user.Email = dt.Rows[0]["correo_electronico"].ToString();
                user.id_usuario = Convert.ToInt32(dt.Rows[0]["id_usuario"].ToString());
            }
            else
            {
                user = null;
            }
            return user;
        }
        public AppUser GetUser(int id)
        {
            List<SqlParameter> lstParametros = new List<SqlParameter>();
            AppUser user = new AppUser();
            lstParametros.Add(new SqlParameter("@id_usuario", id));

            DataTable dt = new DbAcces().Ejecutar_StoredProcedureDT("spr_sel_seg_usuario", lstParametros);
            if (dt.Rows.Count > 0)
            {
                user.Id = Convert.ToInt32(dt.Rows[0]["id_usuario"].ToString());
                user.UserName = Convert.ToString(dt.Rows[0]["usuario"].ToString());
                user.usuario = Convert.ToString(dt.Rows[0]["usuario"].ToString());
                user.descripcion = Convert.ToString(dt.Rows[0]["descripcion"].ToString());
                user.correo_electronico = Convert.ToString(dt.Rows[0]["correo_electronico"]);
                user.Email = dt.Rows[0]["correo_electronico"].ToString();
                user.id_usuario = Convert.ToInt32(dt.Rows[0]["id_usuario"].ToString());
            }
            else
            {
                user = null;
            }
            return user;
        }

        public DbResult AddUser(RegistrationViewModel model)
        {
            List<SqlParameter> lstParametros = new List<SqlParameter>();
            AppUser user = new AppUser();
            DbResult result = new DbResult();
            lstParametros.Add(new SqlParameter("@usuario", model.Email));
            lstParametros.Add(new SqlParameter("@contrasenia", model.Password));
            lstParametros.Add(new SqlParameter("@descripcion", model.LastName));
            lstParametros.Add(new SqlParameter("@correo_electronico", model.Location));
            DataTable dt = new DbAcces().Ejecutar_StoredProcedureDT("spr_ins_usuario", lstParametros);
            DataRow row = dt.Rows[0];
            result.id = Convert.ToInt32(row["id"].ToString());
            result.message = Convert.ToString(row["message"].ToString());
            result.status = Convert.ToString(row["status"].ToString());
            return result;
        }
    }
}
