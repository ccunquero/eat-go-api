﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Security.Models
{
   public class DbResult
    {
        public string status { get; set; }
        public string message { get; set; }
        public int id { get; set; }
    }
}
