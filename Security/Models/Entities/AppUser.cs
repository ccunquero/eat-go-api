﻿ 
using Microsoft.AspNetCore.Identity;

namespace Security.Models.Entities
{
  // Add profile data for application users by adding properties to this class
  public class AppUser : IdentityUser<int>
    {
    // Extended Properties
    public string FirstName { get; set; }

    public string LastName { get; set; }
    public string PictureUrl { get; set; }
    public int id_usuario { get; set; }
    public string usuario { get; set; }
    public string descripcion { get; set; }
    public string correo_electronico { get; set; }
    
  }
}
